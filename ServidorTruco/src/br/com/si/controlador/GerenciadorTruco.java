package br.com.si.controlador;

/**
 * Classe que gerencia os descartes
 *
 */
public class GerenciadorTruco implements OuvinteServico {

    private final ControladorTruco controlador;

    public GerenciadorTruco(ControladorTruco controlador) {
        this.controlador = controlador;
    }

    @Override
    public void servicoExecutado(String ip, String acao) {
        /*Se a ação foi um descarte e não acabou o jogo */
        if (acao.equals("descartar") && !controlador.getJogo().isFimDeJogo()) {

            /*Se a mão está finalizada, então será iniciado uma nova mão*/
            if (controlador.getJogo().getMaoAtual().maoFinalizada()) {
                controlador.servico("gerenciador", ".novaMao");

            } else /*Se a mão está finalizada, então será iniciado uma nova mão*/ {
                if (controlador.getJogo().getMaoAtual().getRodadaAtual().isRodadaFinalizada()) {
                    controlador.servico("gerenciador", ".novaRodada");
                }
            }
        } else if (acao.equals("correr") && !controlador.getJogo().isFimDeJogo()) {

            if (controlador.getJogo().getMaoAtual().maoFinalizada()) {
                controlador.servico("gerenciador", ".novaMao");
            }
        }
    }
}
