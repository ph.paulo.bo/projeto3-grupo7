package br.com.si.execucao;

import br.com.si.comunicacao.RequisicaoRemota;
import br.com.si.comunicacao.Servidor;
import javax.xml.ws.Endpoint;

/**
 *
 *
 * Classe que inicia a aplicação servidora
 *
 */
public class Principal {

    /**
     * *
     * Método que inicia a aplicação.
     *
     * @param args parâmetros de inicialização
     * @throws java.io.IOException Erro de entrada e saída
     */
    public static void main(String... args) throws Exception {
        Endpoint.publish("http://127.0.0.1:9876/requisicao", new RequisicaoRemota());
        Servidor servidor = new Servidor();
        while (true) {
            String ipCliente = servidor.receberConexao();
            new Thread(new RequisicaoRemota(servidor, ipCliente)).start();
        }
    }
}
