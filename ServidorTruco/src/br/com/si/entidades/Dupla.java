package br.com.si.entidades;

import java.io.Serializable;

/**
 * Representa uma dupla no jogo. dupla1 ou dupla2
 */
public enum Dupla implements Serializable {
    Dupla1, Dupla2
}
