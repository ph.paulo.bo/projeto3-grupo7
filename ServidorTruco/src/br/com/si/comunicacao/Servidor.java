package br.com.si.comunicacao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe que cria um servidor encapsulando toda a complexidade de comunicação
 * Cliente/Servidor
 *
 */
public class Servidor {

    /*Conexões ativas com o servidor*/
    private static final Map<String, Socket> clientesConectados = new HashMap<>();
    private final ServerSocket servidor;
    private static int contador = 0;

    /**
     * *
     * Adiciona conexão ao mapa de todas as conexões com o servidor
     *
     * @param socket a ser adicionado a lista de conexões com o servidor
     */
    private void adicionaConexao(String ip, Socket socket) {
        clientesConectados.put(ip, socket);
    }

    public Servidor() throws Exception {
        this.servidor = new ServerSocket(123);
    }

    /**
     * *
     * Coloca o servidor em espera por uma conexão com um cliente.
     *
     * @return String com o ip da máquina conectada
     * @throws java.io.IOException Erro de entrada e saída
     */
    public String receberConexao() throws IOException {
        Socket so = servidor.accept();
        String s = so.getInetAddress().toString();
        if (Servidor.clientesConectados.get(s) != null) {
            ++contador;
            s = s + contador;
        }
        adicionaConexao(s, so);
        return s;
    }

    /**
     * *
     *
     * @param conexao Ip da máquina cliente
     * @return Um stream de saída para ser escrito uma resposta.
     * @throws java.io.IOException Erro de entrada e saída
     */
    public OutputStream getCanalDeSaida(String conexao) throws IOException {
        return clientesConectados.get(conexao).getOutputStream();
    }

    /**
     * *
     *
     * @param conexao Ip da máquina cliente
     * @return Um stream de entrada que recebe mensagens do cliente.
     * @throws java.io.IOException Erro de entrada e saída
     */
    public InputStream getCanalDeEntrada(String conexao) throws IOException {
        return clientesConectados.get(conexao).getInputStream();
    }

    /**
     *
     * @return mapa com as saidas dos clientes conectados
     */
    public static Map<String, OutputStream> getClientesConectados() {
        Map<String, OutputStream> saida = new HashMap<>();
        Servidor.clientesConectados.entrySet().stream().forEach((e) -> {
            try {
                saida.put(e.getKey(), e.getValue().getOutputStream());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        return saida;
    }
}
