/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.modelo.unidade;

import br.com.si.entidades.Carta;
import br.com.si.modelo.Baralho;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author paulo
 */
public class UnidadeBaralho {
    
       
    
    public UnidadeBaralho() {
    }
    
    
    
    @Test
    /**
     * Testa se em um baralho há cartas duplicadas
     */
    public void testeDuplicidadeCarta(){
    
        Baralho b = new Baralho();
        int quantidadeLista = b.getCartas().size();
        Set<Carta> conjunto = new HashSet<>();
        conjunto.addAll(b.getCartas());
        int quantidadeConjunto =  conjunto.size();
        System.out.println(conjunto);
        assertTrue("A quantidade de cartas não deveria ser diferente.", quantidadeConjunto == quantidadeLista);
        
    }
    
}
